package io.services;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.io.*;
import static org.junit.jupiter.api.Assertions.*;

class FileServiceTest {

    private final StreamService streamService = Mockito.mock(StreamService.class);
    private final File f1 = Mockito.mock(File.class);
    private final File f2 = Mockito.mock(File.class);
    private final File f3 = Mockito.mock(File.class);
    private final ByteArrayOutputStream bos = Mockito.mock(ByteArrayOutputStream.class);
    private final FileInputStream is = Mockito.mock(FileInputStream.class);
    private final FileOutputStream os = Mockito.mock(FileOutputStream.class);
    private final File[] files = {f2, f3};
    private final File[] emptyFilesArr = new File[0];
    private final File[] overflowFilesArr = new File[128];
    private final FileService cut = new FileService(streamService);


    @Test
    void overwriteFileTest() throws IOException {
        Mockito.when(streamService.getByteArrayOutputStream()).thenReturn(bos);
        Mockito.when(streamService.getFileInputStream(f2)).thenReturn(is);
        Mockito.when(streamService.getFileInputStream(f3)).thenReturn(is);
        Mockito.when(streamService.getFileOutputStream(f1)).thenReturn(os);

        cut.overwriteFile(f1, files);

        Mockito.verify(streamService, Mockito.times(files.length)).writeToOSFromIS(bos, is);
        Mockito.verify(bos, Mockito.times(1)).writeTo(os);
    }

    @Test
    void overwriteFileExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.overwriteFile(f1, emptyFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToEndFile(f1, overflowFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.overwriteFile(null, emptyFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.overwriteFile(f1, null));
    }

    @Test
    void writeToStartFileTest() throws IOException {
        Mockito.when(streamService.getByteArrayOutputStream()).thenReturn(bos);
        Mockito.when(streamService.getFileOutputStream(f1)).thenReturn(os);
        Mockito.when(streamService.getFileInputStream(f1)).thenReturn(is);
        Mockito.when(streamService.getFileInputStream(f2)).thenReturn(is);
        Mockito.when(streamService.getFileInputStream(f3)).thenReturn(is);

        cut.writeToStartFile(f1, files);

        Mockito.verify(streamService, Mockito.times(3)).writeToOSFromIS(bos, is);
        Mockito.verify(bos, Mockito.times(1)).writeTo(os);
    }

    @Test
    void writeToStartFileExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.writeToStartFile(f1, emptyFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToEndFile(f1, overflowFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToStartFile(null, emptyFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToStartFile(f1, null));
    }

    @Test
    void writeToEndFileTest() throws IOException {
        Mockito.when(streamService.getByteArrayOutputStream()).thenReturn(bos);
        Mockito.when(streamService.getFileOutputStream(f1)).thenReturn(os);
        Mockito.when(streamService.getFileInputStream(f1)).thenReturn(is);
        Mockito.when(streamService.getFileInputStream(f2)).thenReturn(is);
        Mockito.when(streamService.getFileInputStream(f3)).thenReturn(is);

        cut.writeToEndFile(f1, files);

        Mockito.verify(streamService, Mockito.times(3)).writeToOSFromIS(bos, is);
        Mockito.verify(bos, Mockito.times(1)).writeTo(os);
    }

    @Test
    void writeToEndFileExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.writeToEndFile(f1, emptyFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToEndFile(f1, overflowFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToEndFile(null, emptyFilesArr));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToEndFile(f1, null));
    }
}