package io.services;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

class StreamServiceTest {

    private final ByteArrayOutputStream os = Mockito.mock(ByteArrayOutputStream.class);
    private final FileInputStream is = Mockito.mock(FileInputStream.class);
    private final byte[] buffer = new byte[100];
    private final StreamService cut = new StreamService(buffer);

    @Test
    void writeToOSFromISTest() throws IOException {
        int length = 10;
        Mockito.when(is.read(buffer)).thenReturn(length).thenReturn(0);

        cut.writeToOSFromIS(os, is);

        Mockito.verify(os, Mockito.times(1)).write(buffer, 0, length);
    }

    @Test
    void writeToOSFromISExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.writeToOSFromIS(null, is));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToOSFromIS(os, null));
        assertThrows(IllegalArgumentException.class, () -> cut.writeToOSFromIS(null, null));
    }
}