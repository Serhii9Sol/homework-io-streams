package io.services;

import java.io.*;

public class FileService {

    private StreamService streamService;

    public FileService(StreamService streamService) {
        this.streamService = streamService;
    }

    public void overwriteFile(File des, File[] src) {
        if (des == null || src == null || src.length == 0 || src.length > 127) {
            throw new IllegalArgumentException();
        }
        try (ByteArrayOutputStream baos = streamService.getByteArrayOutputStream()) {

            for (int i = 0; i < src.length; i++) {
                if (src[i] == null) {
                    continue;
                }
                try (FileInputStream is = streamService.getFileInputStream(src[i])) {
                    streamService.writeToOSFromIS(baos, is);
                }
            }

            try (FileOutputStream os = streamService.getFileOutputStream(des)) {
                baos.writeTo(os);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToStartFile(File des, File[] src) {
        if (des == null || src == null || src.length == 0 || src.length > 127) {
            throw new IllegalArgumentException();
        }
        try (ByteArrayOutputStream baos = streamService.getByteArrayOutputStream()) {

            for (int i = 0; i < src.length; i++) {
                if (src[i] == null) {
                    continue;
                }
                try (FileInputStream is = streamService.getFileInputStream(src[i])) {
                    streamService.writeToOSFromIS(baos, is);
                }
            }

            try (FileInputStream is = streamService.getFileInputStream(des)) {
                streamService.writeToOSFromIS(baos, is);
            }

            try (FileOutputStream os = streamService.getFileOutputStream(des)) {
                baos.writeTo(os);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void writeToEndFile(File des, File[] src) {
        if (des == null || src == null || src.length == 0 || src.length > 127) {
            throw new IllegalArgumentException();
        }
        try (ByteArrayOutputStream baos = streamService.getByteArrayOutputStream()) {

            try (FileInputStream is = streamService.getFileInputStream(des)) {
                streamService.writeToOSFromIS(baos, is);
            }

            for (int i = 0; i < src.length; i++) {
                if (src[i] == null) {
                    continue;
                }
                try (FileInputStream is = streamService.getFileInputStream(src[i])) {
                    streamService.writeToOSFromIS(baos, is);
                }
            }

            try (FileOutputStream os = streamService.getFileOutputStream(des)) {
                baos.writeTo(os);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
