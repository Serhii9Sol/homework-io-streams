package io.services;

import java.io.*;

public class StreamService {

    private byte[] buffer;

    public StreamService(byte[] buffer) {
        this.buffer = buffer;
    }

    public void writeToOSFromIS(ByteArrayOutputStream to, FileInputStream from) throws IOException {
        if (to == null || from == null) {
            throw new IllegalArgumentException();
        }
        int length;
        while ((length = from.read(buffer)) > 0) {
            to.write(buffer, 0, length);
        }
    }

    public ByteArrayOutputStream getByteArrayOutputStream() {
        return new ByteArrayOutputStream();
    }

    public FileInputStream getFileInputStream(File file) throws IOException {
        return new FileInputStream(file);
    }

    public FileOutputStream getFileOutputStream(File file) throws IOException {
        return new FileOutputStream(file);
    }
}
