package io;

import io.services.FileService;
import io.services.StreamService;
import java.io.File;

public class Main {
    public static void main(String[] args) {
        byte[] buffer = new byte[100];
        StreamService streamService = new StreamService(buffer);
        FileService fileService = new FileService(streamService);

        File f1 = new File("src/main/java/io/files/file1.txt");
        File f2 = new File("src/main/java/io/files/file2.txt");
        File f3 = new File("src/main/java/io/files/file3.txt");
        File f4 = new File("src/main/java/io/files/file4.txt");
        File f5 = new File("src/main/java/io/files/file5.txt");

        File[] files = {f2, f3, f4, f5};

        fileService.overwriteFile(f1, files);
        fileService.writeToStartFile(f1, files);
        fileService.writeToEndFile(f1, files);

    }
}
